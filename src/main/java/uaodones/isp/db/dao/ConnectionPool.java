package uaodones.isp.db.dao;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public final class ConnectionPool {
    private static ConnectionPool instance;

    private static HikariConfig config = new HikariConfig("/hikari.properties");
    private static HikariDataSource ds;

    //private static final Logger log = LogManager.getLogger(String.valueOf(ConnectionPool.class));

    public static ConnectionPool getInstance() {
        if (instance == null) {
            instance = new ConnectionPool();
            ds = new HikariDataSource(config);
        }
        return instance;
    }

    public ConnectionPool() {

    }

    public Connection getConnection() throws SQLException {
        return ds.getConnection();
    }

    public void commitAndClose(Connection connection) {
        try {
            connection.commit();
            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void rollbackAndClose(Connection connection) {
        try {
            connection.rollback();
            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void close(ResultSet resultSet) throws SQLException {
        if (resultSet != null) {
            resultSet.close();
        }
    }
}

