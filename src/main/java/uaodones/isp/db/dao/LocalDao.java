package uaodones.isp.db.dao;

import uaodones.isp.db.entity.impl.Category;
import uaodones.isp.db.exception.DAOException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;

public class LocalDao extends AbstactDao {

    public Category getById(int id) throws DAOException {
        Category category = new Category();
        ResultSet rs = null;
        try (Connection connection = getPool().getConnection();
             PreparedStatement ps = connection.prepareStatement(Query.GET_CATEGORY_BY_ID)) {
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                category.setId(rs.getInt(1));
                category.setName(rs.getString(2));
                category.setLocalId(rs.getInt(3));
            }
        } catch (SQLException e) {
            throw new DAOException("Error Category getById '" + id + "'", e);
        }
        return category;
    }

    public Category getByName(String name) throws DAOException {
        Category category = new Category();
        ResultSet rs = null;
        try (Connection connection = getPool().getConnection();
            PreparedStatement ps = connection.prepareStatement(Query.GET_CATEGORY_BY_NAME)) {
            ps.setString(1, name);
            rs = ps.executeQuery();
            if (rs.next()) {
                category.setId(rs.getInt(1));
                category.setName(rs.getString(2));
                category.setLocalId(rs.getInt(3));
            }
        } catch (SQLException e) {
            throw new DAOException("Error Category getByName '" + name + "'", e);
        } finally {
            close(rs);
        }
        return category;
    }



    public Category create(String name) throws DAOException {
        ResultSet resultSet = null;
        Category mCategory = null;
        if (name != null) {
            try (Connection connection = getPool().getConnection();
                 PreparedStatement preparedStatement = connection.prepareStatement(Query.CREATE_CATEGORY_BY_NAME, Statement.RETURN_GENERATED_KEYS);
            ) {
                mCategory = getByName(name);
                if (mCategory != null) {
                    return mCategory;
                }
                mCategory = new Category();

                preparedStatement.setString(1, name);
                preparedStatement.executeUpdate();
                resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    mCategory.setId(resultSet.getInt(1));
                    mCategory.setName(name);
                }
            } catch (SQLException e) {
                log.log(Level.ALL, e.getMessage());
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                } catch (SQLException e) {
                    log.log(Level.ALL, e.getMessage());
                }
            }
        }
        return mCategory;
    }

    public void update(Category category) {
        PreparedStatement preparedStatement = null;
        try (Connection connection = getPool().getConnection();) {
            preparedStatement = connection.prepareStatement(Query.UPDATE_CATEGORY_BY_ID);
            preparedStatement.setString(1, category.getName());
            preparedStatement.setInt(2, category.getId());
            preparedStatement.setInt(3, category.getLocalId());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            log.log(Level.ALL, e.getMessage());
        } finally {
            close(preparedStatement);
        }

    }
}