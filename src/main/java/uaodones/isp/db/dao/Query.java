package uaodones.isp.db.dao;

public final class Query {
    public static final String GET_ALL = "SELECT * FROM ?";

    public static final String CREATE_CATEGORY_BY_NAME = "INSERT INTO categories(id, name) VALUES( DEFAULT, ?)";
    public static final String GET_ALL_CATEGORIES = "SELECT * FROM categories";
    public static final String GET_CATEGORY_BY_ID = "SELECT * FROM categories WHERE (id = ?)";
    public static final String GET_CATEGORY_BY_NAME = "SELECT * FROM categories WHERE (name = ?)";
    public static final String DELETE_CATEGORY_BY_ID = "DELETE FROM categories WHERE id=?";
    public static final String UPDATE_CATEGORY_BY_ID = "UPDATE categories SET name=? WHERE id=?";
    public static final String TRUNCATE_CATEGORY_BY_ID = "TRUNCATE TABLE categories";
    public static final String POPULATE_CATEGORIES = "INSERT INTO categories (name) VALUES ('Internet'), ('Tv'), ('VOD'); ";

    public static final String CREATE_LOCAL_BY_NAME = "INSERT INTO locales(id, name) VALUES( DEFAULT, ?)";
    public static final String GET_ALL_LOCALES = "SELECT * FROM locales";
    public static final String GET_LOCAL_BY_ID = "SELECT * FROM locales WHERE (id = ?)";
    public static final String GET_LOCAL_BY_NAME = "SELECT * FROM locales WHERE (name = ?)";
    public static final String DELETE_LOCAL_BY_ID = "DELETE FROM locales WHERE id=?";
    public static final String UPDATE_LOCAL_BY_ID = "UPDATE locales SET name=? WHERE id=?";
    public static final String TRUNCATE_LOCALES_BY_ID = "TRUNCATE TABLE locales";
    public static final String POPULATE_LOCALES = "INSERT INTO locales (id, name) VALUES (DEFAULT,'ru'), (DEFAULT,'ua'), (DEFAULT,'en'), (DEFAULT,'pl'); ";

    public static final String GET_USER_BY_LOGIN = "SELECT * FROM categories WHERE (login = ?)";


    public static final String FOREIGN_CHECK_OFF = "SET FOREIGN_KEY_CHECKS = 0";
    public static final String FOREIGN_CHECK_ON = "SET FOREIGN_KEY_CHECKS = 1";
    public static final String CREATE_DB = "";


}