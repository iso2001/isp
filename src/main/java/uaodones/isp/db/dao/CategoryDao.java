package uaodones.isp.db.dao;

import uaodones.isp.db.entity.impl.Category;
import uaodones.isp.db.exception.DAOException;

import java.util.List;

public class CategoryDao {
    public Category getById(int id) throws DAOException {
        return null;
    }

    public List<Category> getAllCategories() {
        return null;
    }

    public Category create(String name) throws DAOException {
        return null;
    }

    public void delete(Category category) {

    }

    public void deleteById(int id) {

    }

}
