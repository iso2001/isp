package uaodones.isp.db.dao;

import uaodones.isp.db.entity.Entity;
import uaodones.isp.db.entity.impl.Category;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class AbstactDao<T> {

    private final ConnectionPool connectionPool = ConnectionPool.getInstance();

    protected static final Logger log = Logger.getLogger(AbstactDao.class.getName());

    public ConnectionPool getPool() {
        return connectionPool;
    }

    public static void close(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                log.log(Level.ALL, e.getMessage());
            }
        }
    }

    public static void close(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                log.log(Level.ALL, e.getMessage());
            }
        }
    }

    public void commitAndClose(Connection con) {
        try {
            con.commit();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void rollbackAndClose(Connection con) {
        try {
            con.rollback();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void delete(Entity entity) {
        deleteById(entity.getId());
    }

    public void deleteById(int id) {
        PreparedStatement statement = null;
        try (Connection connection = getPool().getConnection();) {
            statement = connection.prepareStatement(Query.DELETE_CATEGORY_BY_ID);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (Exception e) {
            log.log(Level.ALL, e.getMessage());
        } finally {
            close(statement);
        }
    }

    public void update(Category category) {
        PreparedStatement preparedStatement = null;
        try (Connection connection = getPool().getConnection();) {
            preparedStatement = connection.prepareStatement(Query.UPDATE_CATEGORY_BY_ID);
            preparedStatement.setString(1, category.getName());
            preparedStatement.setInt(2, category.getId());
            preparedStatement.setInt(3, category.getLocalId());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            log.log(Level.ALL, e.getMessage());
        } finally {
            close(preparedStatement);
        }

    }

}
