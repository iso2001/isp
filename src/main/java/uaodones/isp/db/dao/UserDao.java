package uaodones.isp.db.dao;

import uaodones.isp.db.entity.impl.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDao {
    public User findUserByLogin(String login) {
        User user = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        Connection connection = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            UserMapper mapper = new UserMapper();
            statement = connection.prepareStatement(Query.GET_USER_BY_LOGIN);
            statement.setString(1, login);
            rs = statement.executeQuery();
            if (rs.next()) {
                user = mapper.mapRow(rs);
            }
            rs.close();
            statement.close();
        } catch (SQLException ex) {
            ConnectionPool.getInstance().rollbackAndClose(connection);
            ex.printStackTrace();
        } finally {
            ConnectionPool.getInstance().commitAndClose(connection);
        }
        return user;
    }

    private static class UserMapper implements EntityMapper<User> {

        @Override
        public User mapRow(ResultSet rs) {
            try {
                User user = new User();
                user.setId(rs.getInt("id"));
                user.setName(rs.getString("name"));
                user.setEmail(rs.getString("email"));
                user.setPhoneNumber(rs.getString("phone_number"));
                user.setAddress(rs.getString("address"));
                user.setRoleId(rs.getInt("role_id"));
                user.setLogin(rs.getString("login"));
                user.setPassword(rs.getString("password"));
                user.setLocalId(rs.getInt("local_id"));
                return user;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }


}
