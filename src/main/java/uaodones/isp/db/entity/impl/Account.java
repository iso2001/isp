package uaodones.isp.db.entity.impl;

import uaodones.isp.db.entity.Entity;

import java.sql.Timestamp;

public class Account extends Entity {
    private User user;
    private Timestamp date;
    private String operation;
    private int sum;

    public Account(User user, Timestamp date, String operation, int sum) {
        this.user = user;
        this.date = date;
        this.operation = operation;
        this.sum = sum;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }
}
