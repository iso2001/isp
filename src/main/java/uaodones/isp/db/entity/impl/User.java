package uaodones.isp.db.entity.impl;

import uaodones.isp.db.entity.Entity;

public class User extends Entity {
    private String login;
    private String name;
    private String email;
    private boolean blocked;
    private String phoneNumber;
    private String address;
    private int roleId;
    private String password;


    private int localId;

    public User() {
    }

    public User(String login, String name, String email,
                boolean blocked, String phoneNumber,
                String address, int roleId, String password) {
        this.login = login;
        this.name = name;
        this.email = email;
        this.blocked = blocked;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.roleId = roleId;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int id) {
        this.roleId = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getLocalId() {
        return localId;
    }

    public void setLocalId(int localId) {
        this.localId = localId;
    }
}
