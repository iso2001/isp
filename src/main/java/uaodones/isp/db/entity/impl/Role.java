package uaodones.isp.db.entity.impl;

public enum Role {
    ADMIN, CLIENT;

    public String getName() {
        return name().toLowerCase();
    }

    public static Role getUserRole(User user) {
        int roleId = user.getRoleId();
        return Role.values()[roleId];
    }
}
