package uaodones.isp.db.entity.impl;

public enum Locale {
    ru, en;

    public String getLocalName() {
        return name().toLowerCase();
    }

    public static Locale getUserLocal(User user) {
        int localId = user.getLocalId();
        return Locale.values()[localId];
    }
}
