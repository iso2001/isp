package uaodones.isp.db.entity.impl;

import uaodones.isp.db.entity.Entity;

public class Category extends Entity {
    private String name;
    private int localId;

    public int getLocalId() {
        return localId;
    }

    public void setLocalId(int localId) {
        this.localId = localId;
    }

    public Category() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
