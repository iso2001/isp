package uaodones.isp;

import uaodones.isp.db.dao.CategoryDao;
import uaodones.isp.db.dao.ConnectionPool;
import uaodones.isp.db.dao.Query;
import uaodones.isp.db.entity.impl.Category;
import uaodones.isp.db.exception.DAOException;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class Main {
   // private static final Logger LOGGER = LogManager.getLogger(Main.class);

    public static void main(String[] args) throws SQLException {
        final ConnectionPool connectionPool = ConnectionPool.getInstance();
        Statement statement = null;
        try (Connection connection = connectionPool.getConnection();) {
            statement = connection.createStatement();
            statement.addBatch(Query.FOREIGN_CHECK_OFF);
            statement.addBatch(Query.TRUNCATE_CATEGORY_BY_ID);
            statement.addBatch(Query.TRUNCATE_LOCALES_BY_ID);
            statement.addBatch(Query.FOREIGN_CHECK_ON);
            statement.executeBatch();

            statement.clearBatch();
            statement.addBatch(Query.POPULATE_CATEGORIES);
            statement.addBatch(Query.POPULATE_LOCALES);
            statement.executeBatch();

            CategoryDao categoryDao = new CategoryDao();
            Category category = categoryDao.getById(2);
            System.out.println(category);

            List<Category> categories = categoryDao.getAllCategories();
            if(categories != null) {
                printList(categories);
            }


        } catch (DAOException d) {
            System.err.println(d.getMessage());
        } finally {
            if(statement!=null) {
                statement.close();
            }
        }
    }

    private static void printList(List<?> list) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (Object element : list) {
            sb.append(element.toString());
            sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);
        sb.append("]");
        System.out.println(sb);

    }
}
