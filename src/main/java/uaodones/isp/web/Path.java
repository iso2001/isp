package uaodones.isp.web;

public final class Path {
    public static final String PAGE_LOGIN = "/login.jsp";
    public static final String PAGE_ERROR = "/jsp/error.jsp";

    public static final String COMMAND_ADMIN = "/controller?command=admin";
    public static final String COMMAND_USER = "/controller?command=user";


}
