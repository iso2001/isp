package uaodones.isp.web.command;

import uaodones.isp.web.command.impl.LoginCommand;

import java.util.Map;
import java.util.TreeMap;

public class CommandContainer {
    private static Map<String, Command> commandsMap = new TreeMap();

    static {
        commandsMap.put("login", new LoginCommand());
    }

    public static Command get(String commandName) {
        if (commandName == null || !commandsMap.containsKey(commandName)) {
            return commandsMap.get("noCommand");
        }
        return commandsMap.get(commandName);
    }
}
