package uaodones.isp.web.command.impl;

import uaodones.isp.db.dao.UserDao;
import uaodones.isp.db.entity.impl.Locale;
import uaodones.isp.db.entity.impl.Role;
import uaodones.isp.db.entity.impl.User;
import uaodones.isp.web.Path;
import uaodones.isp.web.command.Command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import java.io.IOException;

public class LoginCommand extends Command {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession session = request.getSession();
        String login = request.getParameter("login");
        String password = request.getParameter("password");

        String errorMessage = null;
        String forwardUrl = Path.PAGE_ERROR;

        if(login == null || password == null || login.isEmpty() || password.isEmpty()) {
            errorMessage = "Login/password is empty.";
            request.setAttribute("errorMessage", errorMessage);
            return forwardUrl;
        }
        User user = new UserDao().findUserByLogin(login);
        if (user == null || !password.equals(user.getPassword())) {
            errorMessage= "Cann't find user with such login/password";
            request.setAttribute("errorMessage", errorMessage);
            return forwardUrl;
        }
        else {
            Role userRole = Role.getUserRole(user);
            if(userRole == Role.ADMIN) {
                forwardUrl = Path.COMMAND_ADMIN;
            }

            if(userRole == Role.CLIENT) {
                forwardUrl = Path.COMMAND_USER;
            }

            session.setAttribute("user", user);
            session.setAttribute("userRole", userRole);

            String userLocaleName = Locale.getUserLocal(user).getLocalName();

            if(userLocaleName != null && !userLocaleName.isEmpty()) {
                Config.set(session, "javax.servlet.jsp.jstl.fmt.locale", userLocaleName);
            }
        }
        return forwardUrl;
    }
}
